package com.springboot.hello.repository;


import com.springboot.hello.entity.SpringUser;
import org.springframework.data.repository.CrudRepository;

public interface SpringUserRepository extends CrudRepository<SpringUser, Long> {

}